import boto3
import os

def lambda_to_sqs(event, context):
    try:
        client = boto3.client('sqs')
        send_msg = client.send_message(
            QueueUrl = os.environ['QUEUE_URL'],
            MessageBody = event['body'],
            DelaySeconds = 0
        )
        response = {
            "statusCode": send_msg['ResponseMetadata']['HTTPStatusCode'],
            "body": send_msg['MessageId']
        }
    except Exception as e:
        response = {
            "statusCode": 500,
            "body": str(e),
        }

    return response

def confirm_webhook(event, context):
    try:
        response = {
            "statusCode": 500,
            "body": ""
        }

        if 'queryStringParameters' in event.keys():
            if 'hub.mode' in event['queryStringParameters'].keys() and event['queryStringParameters']['hub.mode'] == 'subscribe':
                response = {
                    "statusCode": 200,
                    "body": event['queryStringParameters']['hub.challenge']
                }
    except Exception as e:
        response = {
            "statusCode": 500,
            "body": str(e),
        }

    return response